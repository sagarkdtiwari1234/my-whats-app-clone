
// array for holding data of individual chats in singulare objects 

const sagar = [{ img: "himanisarate.jpg", name: "Himani Sarate", sentmessage: "last message", time: "2:45pm", chat: "hello", status:"online",chatsent:"hey" },
{ img: "shalu.jpg", name: "shalu", sentmessage: "last message", time: "2:45pm", chat: "hello daskjgbsd", status:"online"  },
{ img: "pranavGorathe.jpg", name: "Pranav Gorathe", sentmessage: "last message", time: "2:45pm", chat: "hellosdoi", status:"online"  },
{ img: "shivampandey.jpg", name: "Shivam Pandey", sentmessage: "last message", time: "2:45pm", chat: "hello sadofhg", status:"online"  },
{ img: "mannnnnaayaa.jpg", name: "mannnnnaayaa", sentmessage: "last message", time: "2:45pm", chat: "cc", status:"online"  },
{ img: "shreyasi.jpg", name: "Shreyasi", sentmessage: "last message", time: "2:45pm", chat: "dd", status:"online"  },
{ img: "saiii.jpg", name: "Saiiii", sentmessage: "last message", time: "2:45pm", chat: "ff", status:"online"  },
{ img: "ns.jpg", name: "Nikhil Sarate", sentmessage: "last message", time: "2:45pm", chat: "hh", status:"online"  },
{ img: "aarti.jpg", name: "AartiAarti", sentmessage: "last message", time: "2:45pm", chat: "jj", status:"online"  },
{ img: "tara.jpg", name: "Tara", sentmessage: "last message", time: "2:45pm", chat: "rr", status:"online"  },
{ img: "nalayak.jpg", name: "Nalayak", sentmessage: "last message", time: "2:45pm", chat: "yy", status:"online"  },
{ img: "motherindia.jpg", name: "Mother India", sentmessage: "last message", time: "2:45pm", chat: "gg", status:"online"  },
{ img: "pankajsirMTM.jpg", name: "Pankaj sir MTM", sentmessage: "last message", time: "2:45pm", chat: "gg", status:"online"  },
{ img: "simranmammtm.jpg", name: "Simran mam MTM", sentmessage: "last message", time: "2:45pm", chat: "yy" , status:"online" },]


// for loop:- taking data from array if/till the given condition is true
var text = "";
for (i = 0; i < sagar.length; i++) {
    text = text + `<div onclick=myfunction(${i}) class="sidebar-chat">
    <div class="chat-avatar">
        <img src=${sagar[i].img} alt="">
    </div>

    <div class="chat-info">
        <h4>${sagar[i].name}</h4>
        <p>${sagar[i].sentmessage}</p>
    </div>
    <div class="time">
        <p>${sagar[i].time}</p>
    </div>
</div>`
}

// calling out the individual data from loop (i.e. javascript part) and coverting it into HTML in the given div
document.getElementById('sidebar-chats').innerHTML = text;


// the given function is used to change the background color of object when clicked  
function myfunction(selected) {
    function clickChange(selected) {
                className = "sidebar-chat onclick";
        return className;
    }


    var text = "";
    for (i = 0; i < sagar.length; i++) {
        text = text + `<div class="${selected == i ? clickChange(selected) : "sidebar-chat"}"  onclick=myfunction(${i})>
    <div class="chat-avatar">
        <img src=${sagar[i].img} alt="">
    </div>

    <div class="chat-info">
        <h4>${sagar[i].name}</h4>
        <p>${sagar[i].sentmessage}</p>
    </div>
    <div class="time">
        <p>${sagar[i].time}</p>
    </div>
</div>`
    }

    document.getElementById('sidebar-chats').innerHTML = text;

    document.getElementById("header").innerHTML = `
  
   
    <div class="chat-title">
        <div class="avatar">
            <img src="${sagar[selected].img}" alt="">
        </div>
        <div class="message-header-content">
            <h4>${sagar[selected].name}</h4>
            <p>${sagar[selected].status}</p>
        </div>
    </div>
    <div class="chat-header-right">
                <img src="search.svg" alt="">
                <img src="ellipsis-v.svg" alt="">
            </div>`;

    document.getElementById("message-content").innerHTML = `
  
   
            <p class="chat-message">${sagar[selected].chat}<span class="chat-timestamp">${sagar[selected].time}</span></p>,
            
            <p class="chat-message chat-sent">${sagar[selected].chatsent}<span class="chat-timestamp">${sagar[selected].time}</span></p>`;


    if (window.innerWidth <= "950") {
        document.getElementById("invisible").style.display = "none";
        document.getElementById("mediaquery").style.display = "block";

    } else {
        document.getElementById("invisible").style.display = "block";
        document.getElementById("mediaquery").style.display = "block";
    }

}




